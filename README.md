# CT Cars Project

[![pipeline status](https://gitlab.com/ryshard/ct-cars/badges/master/pipeline.svg)](https://gitlab.com/ryshard/ct-cars/commits/master)
---

url: [https://ryshard.gitlab.io/ct-cars/](https://ryshard.gitlab.io/ct-cars/)
---

## Running locally
* Prerequisites: Node.js / npm

### Clone the repo
> git clone https://gitlab.com/ryshard/ct-cars/

> cd ct-cars

### Installation
> npm install

### Run local server
> npm run start

### Testing
> npm test
