export const ENDPOINTS = {
    CARS_LIST: 'https://www.cartrawler.com/ctabe/cars.json',
};

export const ACTION_STATUS = {
    NOT_STARTED: -1,
    IN_PROGRESS: 0,
    SUCCESS: 1,
    ERROR: 2
};

export const ACTION_TYPE = {
    FETCH_CARS: {
        START: 'ON_FETCH_CARS_START',
        SUCCESS: 'ON_FETCH_CARS_SUCCESS',
        ERROR: 'ON_FETCH_CARS_ERROR'
    }
};
