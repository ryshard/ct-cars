import { ENDPOINTS, ACTION_TYPE } from 'constants.js';


export const getCars = (dispatch) => {
    const onSuccess = (response) => {
        if (response.status !== 200) {
            onReject('There was a problemwit request. Status Code: ' + response.status);
            return;
        }
        response.json().then(function (data) {
            dispatch({ type: ACTION_TYPE.FETCH_CARS.SUCCESS, data });
        });

    };

    const onReject = (error) => {
        dispatch({ type: ACTION_TYPE.FETCH_CARS.ERROR, error });
    }

    dispatch({ type: ACTION_TYPE.FETCH_CARS.START });

    fetch(ENDPOINTS.CARS_LIST)
        .then(onSuccess)
        .catch(onReject);
}
