import React from 'react';
import autoBind from 'auto-bind';

class CarListItem extends React.Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }

    expandItem() {
        const { expand, car } = this.props;
        expand(car);
    }

    render() {
        const { car } = this.props;
        const {
            airConditionInd,
            baggageQuantity,
            code,
            codeContext,
            doorCount,
            driveType,
            fuelType,
            model,
            passengerQuantity,
            pictureUrl,
            status,
            transmissionType,
            charge: { total, currency },
            vendor: { name: vendorName }
        } = car;
        const { expand } = this.props;

        return (
            <div className="carListItem">
                <img src={pictureUrl} onClick={this.expandItem} />
                <h2>{model}</h2>
                <dl>
                    <dt>Doors</dt><dd>{doorCount}</dd>
                    <dt>Fuel</dt><dd>{fuelType}</dd>
                    <dt>Transmission</dt><dd>{transmissionType}</dd>
                    <dt>Drive</dt><dd>{driveType}</dd>
                    <dt>Air Contition</dt><dd>{airConditionInd ? 'yes' : 'no'}</dd>
                    <dt>Passangers</dt><dd>{passengerQuantity}</dd>
                    <dt>Baggage Capacity</dt><dd>{baggageQuantity}</dd>
                    <dt>Status</dt><dd>{status}</dd>
                    <dt>Vendor</dt><dd>{vendorName}</dd>
                </dl>
                <h3>{total} {currency}</h3>
                {expand && <a className="btn accent" onClick={this.expandItem}>See more</a>}
            </div >
        );
    }
}

export default CarListItem;
