import React from 'react';
import autoBind from 'auto-bind';

class Location extends React.Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }

    formatDate(dateTime) {
        const date = new Date(dateTime);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        const ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        const strTime = hours + ':' + minutes + ' ' + ampm;
        return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
    }

    render() {
        const {
            pickupLocation,
            returnLocation,
            pickupTime,
            returnTime
        } = this.props.data;
        return (
            <div className="legend">
                <div className="location">
                    <h3>Pick Up</h3>
                    <div className="info">
                        <label>Location: </label>
                        <span>{pickupLocation}</span>
                    </div>
                    <div className="info">
                        <label>Time: </label>
                        <span>{this.formatDate(pickupTime)}</span>
                    </div>
                </div>
                <div className="location">
                    <h3>Return</h3>
                    <div className="info">
                        <label>Location: </label>
                        <span>{returnLocation}</span>
                    </div>
                    <div className="info">
                        <label>Time: </label>
                        <span>{this.formatDate(returnTime)}</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Location;
