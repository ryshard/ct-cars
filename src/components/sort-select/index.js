import React from 'react';
import autoBind from 'auto-bind';
import { path } from 'ramda';
import { lowerCase } from 'lodash';

class SortSelect extends React.Component {
    constructor(props) {
        super(props);
        autoBind(this);
        this.state = { selected: '0' }
    }

    handleChange(e) {
        const selected = path(['target', 'value'], e);
        this.setState({ selected });
        this.props.onSelect(selected);
    }

    renderInput(vendor, i) {
        const { code, name } = vendor;
        const { selected } = this.state;
        const id = lowerCase(name);

        const isSelected = selected === code;

        return (
            <div key={i} className="select-field">
                <label htmlFor={id}>
                    <input type="radio" name="vendor" value={code} id={id} checked={isSelected} onChange={this.handleChange} />
                    {name}
                </label>
            </div>
        );
    }

    render() {
        const { vendors } = this.props;
        return (
            <div className="sort-select">
                <h4>Select your vendor</h4>
                <div className="select-list">
                    {this.renderInput({ code: '0', name: 'All' }, 0)}
                    {vendors.map((vendor, i) => this.renderInput(vendor, i + 1))}
                </div>
            </div >
        );
    }
}

export default SortSelect;
