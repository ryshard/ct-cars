import React from 'react';
import { connect } from 'react-redux';

class Template extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const { children } = this.props;
		return (
			<div className="page" key="1">
				<header>
					<div className="container">
						<h2>CT Cars</h2>
					</div>
				</header>
				<main>
					{children}
				</main>
				<footer>
					<div className="container">
						React.js / redux use case<br />
						by: Gabriel Garus <br />
						gitlab: <a href="https://gitlab.com/ryshard/ct-cars" target="_blank">https://gitlab.com/ryshard/ct-cars</a><br />
						Thank You.
					</div>
				</footer>
			</div>
		);
	}
};

export default Template;
