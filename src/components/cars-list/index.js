import React from 'react';
import autoBind from 'auto-bind';
import CarListItem from 'components/car-list-item';

class CarsList extends React.Component {
    constructor(props) {
        super(props);
        autoBind(this);
        this.state = {
            cars: null
        };
    }

    handleExpand(car) {
        this.setState({ selected: car });
        this.props.onExpand(true);
    }

    closeExpanded() {
        this.setState({ selected: null });
        this.props.onExpand(false);
    }

    renderCars() {
        const { selected } = this.state;
        const { cars } = this.props;

        if (selected || !cars) {
            return null;
        }

        return (
            <div className="cars-list">
                {cars.map((item, i) => (<CarListItem car={item} key={i} expand={this.handleExpand} />))}
            </div>
        );

    }

    renderSelected() {
        const { selected } = this.state;
        if (!selected) {
            return null;
        }
        return (
            <div className="selected">
                <CarListItem car={selected} />
                <button onClick={this.closeExpanded} className="close-btn">X</button>
            </div>
        );
    }


    render() {
        return (
            <div className="listing">
                {this.renderCars()}
                {this.renderSelected()}
            </div>
        );
    }
};

export default CarsList;
