import { extractCarsData } from './helpers';
import { ACTION_STATUS, ACTION_TYPE } from 'constants.js';

export default (state = { carsList: [] }, action = {}) => {
  switch (action.type) {
    case ACTION_TYPE.FETCH_CARS.SUCCESS:
      const carsData = extractCarsData(action.data);
      return {
        ...state,
        ...carsData,
        carsLoadStatus: ACTION_STATUS.SUCCESS
      };


    case ACTION_TYPE.FETCH_CARS.ERROR:
      return {
        ...state,
        carsLoadStatus: ACTION_STATUS.ERROR,
        error: action.data.error
      };

    case ACTION_TYPE.FETCH_CARS.START:
      return {
        ...state,
        carsLoadStatus: ACTION_STATUS.IN_PROGRESS
      };

    default:
      return state
  }
};
