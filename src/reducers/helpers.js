import { path, pathOr } from 'ramda';
import { camelCase, kebabCase } from 'lodash';

export const cleanKeys = (obj) => {
    let newObj = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            let value = obj[key];
            let newKey = camelCase(key.replace('@', ''));
            if (typeof value === "object") {
                value = cleanKeys(value);
            }
            if (newKey === 'vehMakeModel') {
                newKey = 'model';
                value = value.name;
            }
            newObj[newKey] = value;
        }
    }
    return newObj;
}


const extractLcations = (data) => {
    const locationsData = path([0, 'VehAvailRSCore', 'VehRentalCore'], data);
    if (!locationsData) {
        return null;
    }

    return {
        pickupLocation: path(['PickUpLocation', '@Name'], locationsData),
        returnLocation: path(['ReturnLocation', '@Name'], locationsData),
        pickupTime: path(['@PickUpDateTime'], locationsData),
        returnTime: path(['@ReturnDateTime'], locationsData)
    };
}

export const extractCarsData = (data) => {
    let cars = [];
    let vendors = [];
    const locations = extractLcations(data);
    const vendosData = path([0, 'VehAvailRSCore', 'VehVendorAvails'], data);
    if (!vendosData) {
        console.log('No vendors data');
        return { cars, vendors };
    }
    vendosData.forEach(item => {
        const newVendor = {
            name: path(['Vendor', '@Name'], item),
            code: path(['Vendor', '@Code'], item),
        };
        vendors.push(newVendor);
        if (!item.VehAvails) {
            console.log('Missing Data: vehAvails');
            return { cars, vendors };
        }
        item.VehAvails.forEach(veh => {
            const vehDetails = pathOr({}, ['Vehicle'], veh);
            const features = cleanKeys(vehDetails);
            cars.push({
                ...features,
                status: path(['@Status'], veh),
                charge: {
                    total: path(['TotalCharge', '@RateTotalAmount'], veh),
                    estimated: path(['TotalCharge', '@EstimatedTotalAmount'], veh),
                    currency: path(['TotalCharge', '@CurrencyCode'], veh),
                },
                vendor: newVendor
            });
        });

    });


    return { cars, vendors, locations };
};

