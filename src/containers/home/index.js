import React from 'react';
import CarsList from 'components/cars-list';
import SortSelect from 'components/sort-select';
import Location from 'components/location';
import { connect } from 'react-redux';
import { getCars } from 'actions';
import { ACTION_STATUS } from 'constants.js';
import autoBind from 'auto-bind';
import { sort, pathOr, filter, path } from 'ramda';

class Home extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
		this.state = {
			loadError: false,
			loading: false,
			cars: null,
			vendors: null,
			singleExpanded: false
		};
	}

	componentWillReceiveProps(newProps) {
		const { carsLoadStatus: existingLoadStatus } = this.props;
		const { carsLoadStatus: newLoadStatus } = newProps;

		if (existingLoadStatus == ACTION_STATUS.IN_PROGRESS && newLoadStatus == ACTION_STATUS.ERROR) {
			this.setState({ loadError: true, loading: false });
		} else if (existingLoadStatus == ACTION_STATUS.IN_PROGRESS && newLoadStatus == ACTION_STATUS.SUCCESS) {
			this.setState({
				loadError: false,
				loading: false,
				cars: newProps.carsList,
				vendors: newProps.vendorsList,
				locations: newProps.locationsData
			});
		} else if (!existingLoadStatus && newLoadStatus === ACTION_STATUS.IN_PROGRESS) {
			this.setState({ loading: true, loadError: false });
		}

	}

	loadCars() {
		this.props.fetchCars();
	}

	handleSelect(value) {
		if (value === '0') {
			this.setState({ cars: this.props.carsList });
		} else {
			this.setState({ cars: this.filterBy(value) });
		}
	}

	handleExpand(open) {
		this.setState({
			singleExpanded: open
			// cars: this.props.carsList
		});

	}
	filterBy(value) {
		const { carsList } = this.props;
		const isVendor = (car) => {
			const vendorCode = path(['vendor', 'code'], car);
			return vendorCode === value;
		};
		return filter(isVendor, carsList);
	}

	sort(cars) {
		var diff = function (a, b) {
			const valueA = pathOr(0, ['charge', 'total'], a);
			const valueB = pathOr(0, ['charge', 'total'], b);
			return valueA - valueB;
		};
		return sort(diff, cars);
	}
	render() {
		const {
			cars,
			vendors,
			locations,
			singleExpanded,
			loading
		} = this.state;
		const btnClass = cars ? 'btn accent disabled' : 'btn accent';
		return (
			<div className="container">
				<h1>Our Cars</h1>
				<button className={btnClass} onClick={this.loadCars} disabled={cars}>Load Cars</button>
				{loading && <div className="loader"></div>}
				{locations && <Location data={locations} />}
				{vendors && !singleExpanded && <SortSelect vendors={vendors} onSelect={this.handleSelect} />}
				{cars && <CarsList cars={this.sort(cars)} onExpand={this.handleExpand} />}
			</div>
		);
	}
};


const mapStateToProps = state => ({
	carsList: state.carsReducer.cars,
	vendorsList: state.carsReducer.vendors,
	locationsData: state.carsReducer.locations,
	carsLoadStatus: state.carsReducer.carsLoadStatus
});

const mapDispatchToProps = dispatch => ({
	fetchCars: () => getCars(dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
